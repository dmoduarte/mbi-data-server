
var express = require('express');
var http = require('http');
var app = express();
var httpServer = http.createServer(app);	
var fs = require('fs');
var pg = require('pg');
var bodyParser = require('body-parser');
var connectionURLGALP = "postgres://postgres:postgres@localhost:5432/GalpMobile";
var connectionURLELECTRA = "postgres://postgres:postgres@localhost:5432/ElectraMobile";

//environment setting
app.set('port', (process.env.PORT || 3000));
var clientGALP = new pg.Client(connectionURLGALP);
var clientELECTRA = new pg.Client(connectionURLELECTRA);
clientGALP.connect();
clientELECTRA.connect();
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false })

httpServer.listen(app.get('port'), function () {
    console.log("Express server listening on port %s.", httpServer.address().port);
});

var self = this;
self.executeQuery = function(querySTR,res,client){
    // for heroku use this: process.env.DATABASE_URL+'?ssl=true'
    var query = client.query(querySTR, function(err,result){
        res.json(result.rows);
    });
};

app.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.all('/retrieveData', urlencodedParser, function(req, res) {
	var query = req.body.query;
	console.log(query);
	clientELECTRA.query(query, function(err,result){
		 if(err == null ){	 
			 var data = JSON.parse(JSON.stringify(result.rows));
			 res.json(data);
		 }
		else
			res.json(err);
	});
	
});
	
app.all('/metadataGALP', urlencodedParser, function(req, res) {
   var clientLastUpdate = req.body.last_update;
   var template = '{"name": "MobileBI-metadata","description": "Dummy data GALP","remoteURL": "/metadataGALP", "last_update":';
   var templateModules =  ',"modules":';
   var end = '}';
   var selectMax  = "SELECT max(last_process) as last_update FROM CONFIG.PROCESS_METADATA";
   var query = 'SELECT * FROM CONFIG.PROCESS_METADATA WHERE last_process > '+clientLastUpdate;
   var hardCodedDims =  [{"resource": "DIM_ESTADOS","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_LOCAL_ATENDIMENTO","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_TIPO_ENCOMENDA","table_schema": "public","data_source": "n/a"}
							     ];
						
   console.log(req.body.last_update);
   
   clientGALP.query(selectMax, function(err,result){
		var max = JSON.parse(JSON.stringify(result.rows[0]));
		clientGALP.query(query, function(err,result){
			if(result.rows.length > 0){
				var rows = result.rows;
				var jsonString = JSON.stringify(rows);
				var jsonObj = JSON.parse(jsonString);
				
				for(i = 0; i < hardCodedDims.length;i++)
					jsonObj.push(hardCodedDims[i]);
				
				var metadata = template + JSON.stringify(max.last_update) + templateModules + JSON.stringify(jsonObj) + end;
				res.json(JSON.parse(metadata));
		}else{
			var metadata = template + JSON.stringify(max.last_update) + templateModules +JSON.stringify(hardCodedDims) + end;
			res.json(JSON.parse(metadata));
			}				
		});	
    });

});




app.all('/metadataELECTRA', urlencodedParser, function(req, res) {
   var clientLastUpdate = req.body.last_update;
   var template = '{"name": "MobileBI-metadata","description": "Dummy data Electra","remoteURL": "/metadataELECTRA", "last_update":';
   var templateModules =  ',"modules":';
   var end = '}';
   var selectMax  = "SELECT max(last_process) as last_update FROM CONFIG.PROCESS_METADATA";
   var query = 'SELECT * FROM CONFIG.PROCESS_METADATA WHERE last_process > '+clientLastUpdate;
   var hardCodedDims =  [{"resource": "DIM_CLIENT_TYPES","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_COMPLAINT_TYPES","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_MANUFACTURERS","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_PERIOD","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_PRODUCTS","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_REGION","table_schema": "public","data_source": "n/a"},
								  {"resource": "DIM_SECTORS","table_schema": "public","data_source": "n/a"}
							     ];
						
   console.log(req.body.last_update);
   
   clientELECTRA.query(selectMax, function(err,result){
		var max = JSON.parse(JSON.stringify(result.rows[0]));
		clientELECTRA.query(query, function(err,result){
			if(result.rows.length > 0){
				var rows = result.rows;
				var jsonString = JSON.stringify(rows);
				var jsonObj = JSON.parse(jsonString);
				
				for(i = 0; i < hardCodedDims.length;i++)
					jsonObj.push(hardCodedDims[i]);
				
				var metadata = template + JSON.stringify(max.last_update) + templateModules + JSON.stringify(jsonObj) + end;
				res.json(JSON.parse(metadata));
		}else{
			var metadata = template + JSON.stringify(max.last_update) + templateModules +JSON.stringify(hardCodedDims) + end;
			res.json(JSON.parse(metadata));
			}				
		});	
    });

});

app.get('/', function(req, res) {
   res.send('Hello Human!');
});


//ELECTRA
app.all('/DIM_REGION', function(req, res) {
    //self.executeQuery("SELECT * FROM REGION,res);
   var data = JSON.parse(fs.readFileSync("./data/region_data.json",'utf8'));
   res.jsonp(data);
});

app.all('/DIM_COMPLAINT_TYPES', function(req, res) {
   var query = 'SELECT id,name FROM complaintTypes'
   console.log(query);
   self.executeQuery(query,res,clientELECTRA);
});

app.all('/DIM_MANUFACTURERS', function(req, res) {
   var query = 'SELECT id,name FROM manufacturers'
    console.log(query);
   self.executeQuery(query,res,clientELECTRA);
});

app.all('/DIM_PERIOD', function(req, res) {
   var query = 'SELECT id,month,year FROM periods'
    console.log(query);
   self.executeQuery(query,res,clientELECTRA);
});

app.all('/DIM_PRODUCTS', function(req, res) {
   var query = 'SELECT id,name FROM products'
    console.log(query);
   self.executeQuery(query,res,clientELECTRA);
});


app.all('/DIM_SECTORS', function(req, res) {
   var query = 'SELECT id,name FROM sectors'
    console.log(query);
   self.executeQuery(query,res,clientELECTRA);
});

app.all('/DIM_CLIENT_TYPES', function(req, res) {
   var query = 'SELECT id,name FROM clientTypes'
    console.log(query);
   self.executeQuery(query,res,clientELECTRA);
});

app.post('/FCT_ANOMALIES',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate);
  
  var query = "SELECT * FROM anomalies where etl_proc > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientELECTRA);  
});

app.post('/FCT_BILLINGS',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate);
  
  var query = "SELECT * FROM billings where etl_proc > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientELECTRA);  
});

app.post('/FCT_COMPLAINTS',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate);
  
  var query = "SELECT * FROM complaints where etl_proc > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientELECTRA);  
});

app.post('/FCT_CONTRACTING',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate);
  
  var query = "SELECT * FROM contracting where etl_proc > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientELECTRA);  
});

app.post('/FCT_DEBTS',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate);
  
  var query = "SELECT * FROM debts where etl_proc > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientELECTRA);  
});

app.post('/FCT_PROFIT',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate + ' PROFIT');
  
  var query = "SELECT * FROM profit where etl_proc > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientELECTRA);  
});


//GALP
app.post('/FCT_ENCOMENDAS',urlencodedParser,function(req, res) {
  var clientLastUpdate = req.body.last_update;
  console.log(clientLastUpdate);
  
  var query = "SELECT * FROM topaggregate where last_update > "+clientLastUpdate;
  console.log(query);
  self.executeQuery(query,res,clientGALP);  
});

app.all('/DIM_ESTADOS', function(req, res) {
  var query = "SELECT id_estado_encomenda, ds_estado_encomenda FROM DIM_ESTADOS";
  self.executeQuery(query,res,clientGALP);  
});

app.all('/DIM_TIPO_ENCOMENDA', function(req, res) {
  var query = "SELECT id_tipo_encomenda, ds_tipo_encomenda FROM DIM_TIPO_ENCOMENDA";
  self.executeQuery(query,res,clientGALP);  
});

app.all('/DIM_LOCAL_ATENDIMENTO', function(req, res) {
  var query = "SELECT id_local_atendimento, ds_local_atendimento FROM DIM_LOCAL_ATENDIMENTO";
  self.executeQuery(query,res,clientGALP);  
});



